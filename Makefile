CXX=g++-5

CXXFLAGS =	 -fopenmp -O3 -g -Wall -std=c++1z

OBJS =		BarnesHutCPU.o

LIBS =

TARGET =	BarnesHutCPU

$(TARGET):	$(OBJS)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJS) $(LIBS) -lboost_program_options

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)
