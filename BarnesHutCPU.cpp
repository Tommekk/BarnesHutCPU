//============================================================================
// Name        : BarnesHutCPU.cpp
// Author      : Dominik Thomas
// Version     :
// Copyright   : Copyright 2016 Dominik Thomas
// Description : Hello World in C, Ansi-style
//============================================================================

#include <boost/program_options.hpp>


#include <iostream>
#include <cmath>
#include <random>
#include <map>
#include <fstream>
#include <string>
#include <mutex>
#include <chrono>

// TODO add verbose mode

//#define INFO_MODE // Used for infos
#define USE_CPU
#define USE_PARALLEL
//#define USE_MINMAX_ELEMENT

#ifdef USE_CPU
#include <vector>
#include <unordered_map>
#include <boost/iterator/counting_iterator.hpp>

template <typename T>
using counting_iterator = boost::counting_iterator<T>;

#ifdef USE_PARALLEL
#include <parallel/algorithm>

namespace policy = __gnu_parallel;
#else
#include <algorithm>
#include <queue>

namespace policy = std;
#endif

#endif // TODO else USE GPU

#ifdef USE_GPU
#include <thrust/for_each.h>
#endif


//#ifdef USE_PAR_EXECUTION_POLICY
//#define EXECUTION_POLICY std::execution::par
//#else
//#define EXECUTION_POLICY std::execution::seq
//#endif

namespace po = boost::program_options;



namespace BH {



// TODO ifdef USE_GPU

// Datatypes
/*
//                    min_x, max_x, min_y, max_y, min_z, max_z
typedef thrust::tuple<double, double, double, double, double, double> bbox_type; // boundarybox

typedef std::vector<bbox_type> bboxvec_type;
// position vector
//                               x                               y                             z
typedef thrust::tuple<std::vector<double>, std::vector<double>, std::vector<double> > posvec_type;
// velocity vector
//                             v_x                             v_y                           v_z
typedef thrust::tuple<std::vector<double>, std::vector<double>, std::vector<double> > velvec_type;
 */




// Build tree


int N=200;//1200; // TODO remove comments
int timesteps=500; // 500
bool use_timer;
// TODO change
//const double G = 6.67408e-11;
const double G = 1;

// TODO replace .at functions by operator[], because then no bounds checkings are active (faster)
// TODO code analyzer
// TODO make viewer

std::vector<double> mass(N);
std::vector<double> x(N);
std::vector<double> y(N);
std::vector<double> z(N);
std::vector<double> v_x(N);
std::vector<double> v_y(N);
std::vector<double> v_z(N);
std::vector<double> a_x(N);
std::vector<double> a_y(N);
std::vector<double> a_z(N);

// bbox
// min_x, max_x, min_y, max_y, min_z, max_z, mass (comes later)
/* std::vector<double> min_x(N_max), max_x(N_max), min_y(N_max), max_y(N_max), min_z(N_max), max_z(N_max);
std::vector<int> node_type(N_max);
std::vector<double> node_mass(N_max);
std::vector<double> node_mass_x(N_max);
std::vector<double> node_mass_y(N_max);
std::vector<double> node_mass_z(N_max); */
// TODO replace map by tuple
std::unordered_map<int, double> min_x, max_x, min_y, max_y, min_z, max_z;
std::unordered_map<int, int> node_type;
std::unordered_map<int, double> node_mass;
std::unordered_map<int, double> node_mass_x;
std::unordered_map<int, double> node_mass_y;
std::unordered_map<int, double> node_mass_z;


void buildTree(std::vector<double> &x, std::vector<double> &y, std::vector<double> &z, int N)
{ // TODO maybe build tree after sorting elements
	// positiv means leave refering to index in particles, -1 is bound with children, node does not exist: without children
	// add zero at last or rename to N maybe

	min_x.clear();	max_x.clear();
	min_y.clear();	max_y.clear();
	min_z.clear();	max_z.clear();
	node_type.clear(); node_mass.clear();
	node_mass_x.clear(); node_mass_y.clear(); node_mass_z.clear();

	// TODO add Variable at config
#ifdef USE_MINMAX_ELEMENT
	auto minmax_x = std::minmax(x.begin(), x.end());
	auto minmax_y = std::minmax(y.begin(), y.end());
	auto minmax_z = std::minmax(z.begin(), z.end());

	min_x[0] = *minmax_x.first;
	max_x[0] = *minmax_x.second;
	min_y[0] = *minmax_y.first; // TODO throws segmentation error?
	max_y[0] = *minmax_y.second;
	min_z[0] = *minmax_z.first;
	max_z[0] = *minmax_z.second;
#else
	min_x[0] = *std::min_element(x.begin(), x.end());
	max_x[0] = *std::max_element(x.begin(), x.end());
	min_y[0] = *std::min_element(y.begin(), y.end());
	max_y[0] = *std::max_element(y.begin(), y.end());
	min_z[0] = *std::min_element(z.begin(), z.end());
	max_z[0] = *std::max_element(z.begin(), z.end());
#endif

	// TODO use LOG library
	//std::cout << "Min:" << min_x[0] << "," << min_y[0] << "," << min_z[0] << std::endl;
	//std::cout << "Max:" << max_x[0] << "," << max_y[0] << "," << max_z[0] << std::endl;

	node_type[0] = 0;

	std::mutex m_insert, m_move;

	// TODO elemente könnte man so vorsortieren, dass nur wenige Konflikte entstehen?


	// don't start from 0 and first element already added (see above)
	policy::for_each(counting_iterator<int>(1), counting_iterator<int>(N), [&](const int &i){
		// add Knot to existing tree
		int j = 0;
		while(true) {
			// if position is empty add
			if(node_type.count(j) == 0) { // node does not exists
				std::lock_guard<std::mutex> lock(m_insert);
				// TODO maybe use condition variable
				//If something changed before the lock continue
				if (node_type.count(j) != 0) {
					continue;
				}
				// insert particle index
				node_type[j] = i; // refering to particle index
				// update min_x, max_x....
				double new_min_x, new_max_x, new_min_y, new_max_y, new_min_z, new_max_z;
				int parent_j = (j-1)/8;

				double half_x = (max_x[parent_j]+min_x[parent_j]) /2;
				double half_y = (max_y[parent_j]+min_y[parent_j]) /2;
				double half_z = (max_z[parent_j]+min_z[parent_j]) /2;

				if(x[i] < half_x) {
					new_min_x = min_x[parent_j];
					new_max_x = half_x;
				} else {
					new_min_x = half_x;
					new_max_x = max_x[parent_j];
				}
				if(y[i] < half_y) {
					new_min_y = min_y[parent_j];
					new_max_y = half_y;
				} else {
					new_min_y = half_y;
					new_max_y = max_y[parent_j];
				}
				if(z[i] < half_z) {
					new_min_z = min_z[parent_j];
					new_max_z = half_z;
				} else {
					new_min_z = half_z;
					new_max_z = max_z[parent_j];
				}
				min_x[j] = new_min_x;
				max_x[j] = new_max_x;
				min_y[j] = new_min_y;
				max_y[j] = new_max_y;
				min_z[j] = new_min_z;
				max_z[j] = new_max_z;
				break;
			}
			if(node_type[j] == -1) { // contains more subelements
				// TODO throw error if particles are at same point
				// TODO abort if particles are too close

				// calc next Octant positions
				double half_x = (max_x[j]+min_x[j])/2;
				double half_y = (max_y[j]+min_y[j])/2;
				double half_z = (max_z[j]+min_z[j])/2;

				int k = 1;
				if(x[i] < half_x) k += 1;
				if(y[i] < half_y) k += 2;
				if(z[i] < half_z) k += 4;

				j = 8*j+k;
				continue;
			}
			if(node_type[j] >= 0) { // node contains only leave!
				std::lock_guard<std::mutex> lock(m_move);
				// TODO maybe use condition variable
				//If something changed before the lock continue
				if (!(node_type[j] >= 0)) {
					continue;
				}
				int old_i = node_type[j];
				int k1 = 1;

				// subdivide node
				double half_x = (max_x[j]+min_x[j])/2;
				double half_y = (max_y[j]+min_y[j])/2;
				double half_z = (max_z[j]+min_z[j])/2;
				if(x[old_i] < half_x) k1 += 1;
				if(y[old_i] < half_y) k1 += 2;
				if(z[old_i] < half_z) k1 += 4;
				// TODO make function
				int new_particle_pos = 8*j+k1;
				//std::vector<int>::iterator it = node_type.begin();
				//std::advance(it, new_particle_pos);
				//node_type.insert(it, old_i); // move exisiting leave
				node_type[new_particle_pos] = old_i;
				// set ranges for new subelement
				double new_min_x, new_max_x, new_min_y, new_max_y, new_min_z, new_max_z;
				if(x[old_i] < half_x) {
					new_min_x = min_x[j];
					new_max_x = half_x;
				} else {
					new_min_x = half_x;
					new_max_x = max_x[j];
				}
				if(y[old_i] < half_y) {
					new_min_y = min_y[j];
					new_max_y = half_y;
				} else {
					new_min_y = half_y;
					new_max_y = max_y[j];
				}
				if(z[old_i] < half_z) {
					new_min_z = min_z[j];
					new_max_z = half_z;
				} else {
					new_min_z = half_z;
					new_max_z = max_z[j];
				}
				min_x[new_particle_pos] = new_min_x;
				max_x[new_particle_pos] = new_max_x;
				min_y[new_particle_pos] = new_min_y;
				max_y[new_particle_pos] = new_max_y;
				min_z[new_particle_pos] = new_min_z;
				max_z[new_particle_pos] = new_max_z;

				// set node now contains more subelements
				node_type[j] = -1;

				// new pos to add particle
				int k = 1;
				if(x[i] < half_x) k += 1;
				if(y[i] < half_y) k += 2;
				if(z[i] < half_z) k += 4;

				j = 8*j+k;
				continue;
			}

			// TODO throw error
			std::cerr << "Error in building tree!" << std::endl;
		}
	});
}

int getLevel1(int i)
{
	//if(i == 0) return i;
	//return std::floor(std::log(i  - (i-1)%8)/std::log(8)+1);
	//return (std::log((i-1)*8)/std::log(8));
	// calc level by calculating ranges of each height
	// TODO use O(1) formula
	for (int h = 0; true; ++h) {
		int min_i = (std::pow(8, h) - 1)/(8-1);
		int max_i = (std::pow(8, h+1) - 1)/(8-1);
		if(min_i <= i && i < max_i ) return h;
	}
}

int getLevel2(int i)
{
	return std::floor(std::log(7*i+1)/std::log(8));
}

int getChildNum(int i)
{
	if(i == 0) return i;
	return (i - 1) % 8;
}

void calcNodeMasses2() // TODO maybe use lazy calculation instead
{
	// std::cout << "Highest parent_j:"  << highest_parent_j << std::endl;

	std::priority_queue<int> q; // TODO will not work on gpu due to missing queue data type
	for (auto it = node_type.begin(); it != node_type.end(); ++it) {
		auto tmp = it->first;
		q.push(tmp);
	}

	// TODO only calc where necessaryx
	// TODO make parallel by down to up method?
	// TODO use partial sum?
	while (!q.empty()) { // TODO try to use transform
		int j = (q.top()-1)/8; // TODO make parent j call function
		q.pop();
		if (j <= 0) return; // At end of priority queue
		//	if (node_type.count(j) > 0) continue; // already calculated (two items in queue)
		double mass_sum = 0;
		double mass_x, mass_y, mass_z;
		mass_x = mass_y = mass_z = 0;
		for(int k = 1; k <= 8; ++k) { // TODO calc mass center point
			int i = 8*j+k;
			if(node_type.count(i) == 0) continue; // No entry in node_type exists so skip
			if(node_type[i] == -1) {// subelements exists so use t
				// TODO exception here because i=808423375
				// TODO "merge" masses to one if to close, just skip calculating then set Max_i and min_dist
				double subset_mass = node_mass.at(i); // TODO merge particle_mass and node_mass
				mass_x += subset_mass*node_mass_x[i];
				mass_y += subset_mass*node_mass_y[i];
				mass_z += subset_mass*node_mass_z[i];
				mass_sum += subset_mass;
				continue;
			}
			//if( node_type[i] >= 0) { // normal particle
			int particle_i = node_type[i];
			double particle_mass = mass[node_type[i]];
			mass_x += particle_mass*x[particle_i];
			mass_y += particle_mass*y[particle_i];
			mass_z += particle_mass*z[particle_i];
			mass_sum += particle_mass;
		}
		if(mass_sum == 0) continue; // skip if mass sum is 0
		mass_x /= mass_sum;
		mass_y /= mass_sum;
		mass_z /= mass_sum;
		node_mass[j] = mass_sum;
		node_mass_x[j] = mass_x;
		node_mass_y[j] = mass_y;
		node_mass_z[j] = mass_z;
		q.push(j);
	}
}

void calcNodeMasses1() // TODO maybe use lazy calculation instead
{
	// max j
	//int highest_parent_j = ((--node_type.end())->first-1)/8; // TODO make function
	// std::cout << "Highest parent_j:"  << highest_parent_j << std::endl;

	int highest_parent_j = 0;
	for( const auto& n : node_type ) {
		if(n.first > highest_parent_j) highest_parent_j = n.first;
	}
	highest_parent_j  = (highest_parent_j - 1) / 8;


	// TODO only calc where necessaryx
	// TODO make parallel by down to up method?
	// TODO use partial sum
	for (int j = highest_parent_j; j >= 0; --j) { // TODO try to use transform/reduce/prefix_sum
		double mass_sum = 0;
		double mass_x, mass_y, mass_z;
		mass_x = mass_y = mass_z = 0;
		for(int k = 1; k <= 8; ++k) { // TODO calc mass center point
			int i = 8*j+k;
			if(node_type.count(i) == 0) continue; // No entry in node_type exists so skip
			if(node_type[i] == -1) {// subelements exists so use t
				double subset_mass = node_mass.at(i); // TODO merge particle_mass and node_mass
				mass_x += subset_mass*node_mass_x[i];
				mass_y += subset_mass*node_mass_y[i];
				mass_z += subset_mass*node_mass_z[i];
				mass_sum += subset_mass;
				continue;
			}
			//if( node_type[i] >= 0) { // normal particle
			int particle_i = node_type[i];
			double particle_mass = mass[node_type[i]];
			mass_x += particle_mass*x[particle_i];
			mass_y += particle_mass*y[particle_i];
			mass_z += particle_mass*z[particle_i];
			mass_sum += particle_mass;
		}
		if(mass_sum == 0) continue; // skip if mass sum is 0
		mass_x /= mass_sum;
		mass_y /= mass_sum;
		mass_z /= mass_sum;
		node_mass[j] = mass_sum;
		node_mass_x[j] = mass_x;
		node_mass_y[j] = mass_y;
		node_mass_z[j] = mass_z;

	}
}

// TODO use std::hypot?
double hypot(double x, double y, double z)
{
	return std::sqrt(x*x+y*y+z*z);
}

void calcAcc() // TODO at the moment acc is not linear
{
	double theta = 0.5; // TODO make theta changeable by user
	// TODO use vector as queue, because thrust has no queue-type
	policy::for_each(counting_iterator<int>(0), counting_iterator<int>(N), [&](const int &i){
		std::vector<int> queue;
		double pos_x = x.at(i);
		double pos_y = y.at(i);
		double pos_z = z.at(i);

		double acc_x, acc_y, acc_z;
		acc_x = acc_y = acc_z = 0;

		for (int j = 1; j < 9; ++j) {
			queue.push_back(j);
		}
		while(!queue.empty()) {// TODO try to make parallel?
			// calc distance to mass center
			int j = queue.back();
			queue.pop_back();
			// no nodes in this area, so skip
			if (node_type.count(j) == 0) continue;
			// if particle, calc acc directly
			double mass2, mass_x, mass_y, mass_z, d;
			double d_x, d_y, d_z;

			if (node_type.at(j) == -1) {
				mass_x = node_mass_x.at(j);
				mass_y = node_mass_y.at(j);
				mass_z = node_mass_z.at(j);
				d_x = mass_x-pos_x;
				d_y = mass_y-pos_y;
				d_z = mass_z-pos_z;
				d = hypot(d_x, d_y, d_z);

				// calc bbox length
				double bbow_width = max_x.at(j)-min_x.at(j);
				// greater theta than add sub elements to queue
				if (bbow_width / d > theta) {// TODO check if uses
					for (int k = 1; k <= 8; ++k) {
						queue.push_back(8*j+k);
					}
					// do nothing if subelements have to be used;
					continue;
				} // TODO check if used
				mass2 = node_mass.at(j);
				// else calc acceleration
			} else {// else (node_type.at(j) >= 0, therefore direct particle
				int par_i = node_type.at(j);
				// don't calc for own particle
				if(par_i == i) continue;
				mass2 = mass.at(par_i);
				mass_x = x.at(par_i);
				mass_y = y.at(par_i);
				mass_z = z.at(par_i);
				d_x = mass_x-pos_x;
				d_y = mass_y-pos_y;
				d_z = mass_z-pos_z;
				d = hypot(d_x, d_y, d_z);
			}
			// calc acceleration
			double factor = G*mass2/std::abs(std::pow(d, 3));
			// and add it
			acc_x += factor * d_x;
			acc_y += factor * d_y;
			acc_z += factor * d_z;
		}
		a_x[i] = acc_x;
		a_y[i] = acc_y;
		a_z[i] = acc_z;
	});
}

void updateData()
{
	// TODO make timesteplength changeable
	double dt = 10; //s

	// alternative to counting iterator is vector with elements from 0 to N
	// TODO use tabulate
	policy::for_each(boost::counting_iterator<int>(0), boost::counting_iterator<int>(N), [&](const int i){
		v_x[i] += a_x[i]*dt;
		v_y[i] += a_y[i]*dt;
		v_z[i] += a_z[i]*dt;

		x[i] += v_x[i]*dt;
		y[i] += v_y[i]*dt;
		z[i] += v_z[i]*dt;
	});
}



void printTrees()
{
	std::cout << "Tree, Size: " << node_type.size() << std::endl;

	// node_type
	for(auto it = node_type.begin(); it != node_type.end(); ++it) {
		// particle, octtree range, level, and number
		std::cout << "Node: " << it->first << ", Position: " << getLevel2(it->first)  << "." << getChildNum(it->first) << ", PartRef " <<  it->second <<
				std::endl;
	}



	// masses
	std::cout << "Masses, Size: " << node_mass.size() << ", {" << node_mass_x.size() << ", " << node_mass_y.size()  << ", " << node_mass_z.size() << "}"  << std::endl;
	for(auto it = node_mass.begin(); it != node_mass.end(); ++it) {
		std::cout << "Node: " << it->first << ", Position: " << getLevel2(it->first)  << "." << getChildNum(it->first) << ", Mass " <<  it->second <<
				" {" << node_mass_x.at(it->first) << ", " << node_mass_y.at(it->first) << ", " << node_mass_z.at(it->first)  << "}"  <<
				std::endl;
	}


}

void printParticleData()
{
	for(int i = 0; i < N; i++) 	{
		std::cout << i << ". Mass:" << mass[i];
		std::cout << " {" << x[i] << "," << y[i] << ","<< z[i] << "}";
		std::cout << " v{" << v_x[i] << "," << v_y[i] << ","<< v_z[i] << "}";
		std::cout << " a{" << a_x[i] << "," << a_y[i] << ","<< a_z[i] << "}" << std::endl;
	}
}

void generateRandomEntrys() // TODO replace with thrust tabulate
{
	std::srand(0);
	std::default_random_engine gen(6);
	std::uniform_real_distribution<> dis1(-500000, 500000);
	std::uniform_real_distribution<> dis2(-1, 1);
	std::uniform_real_distribution<> dis3(1000000, 5000000);
	std::uniform_real_distribution<> dis4(-M_PI, M_PI);
	std::uniform_real_distribution<> dis5(-100, 100);
	auto random_pos = [&](){return dis1(gen);};
	auto random_vel = [&](){return dis2(gen);};
	//auto return_zero = [](){return 0;};
	// auto rdis2 = [&](){return dis2(gen);};
	//auto rdis3 = [](){return 3;};//[&](){return dis3(gen);};

	auto rdis6 = [&](){return dis3(gen);};//[&](){return dis3(gen);};

	//auto rdis5 = [](){return 3;};//[&](){return dis3(gen);}


	// mass[0] = 100000;
	// x[0] = y[0] = z[0] = v_x[0] = v_y[0] = v_z[0] = 0;
	/*
    for (int i = 1; i < N; ++i) {
		mass[i] = 3;
		double r = dis3(gen)+50;
		double phi = dis4(gen);
		x[i] = r*cos(phi);
		y[i] = r*sin(phi);
		z[i] = dis5(gen);
		double v = std::sqrt(5000/r)/10;
		v_x[i] = v*sin(phi);
		v_y[i] = v*cos(phi);
		v_z[i] = 0;
	}*/

	// Higher Masses
	/*	std::generate(x.begin(), x.begin()+5, rdis1);
	std::generate(y.begin(), y.begin()+5, rdis1);
	std::generate(z.begin(), z.begin()+5, rdis3);
	std::generate(v_x.begin(), v_x.begin()+5, rdis2);
	std::generate(v_y.begin(), v_y.begin()+5, rdis2);
	std::generate(v_z.begin(), v_z.begin()+5, rdis2);
	std::generate(mass.begin(), mass.begin()+5, rdis6); */



	std::generate(x.begin(), x.end(), random_pos);
	std::generate(y.begin(), y.end(), random_pos);
	std::generate(z.begin(), z.end(), random_pos);
	std::generate(v_x.begin(), v_x.end(), random_vel);
	std::generate(v_y.begin(), v_y.end(), random_vel);
	std::generate(v_z.begin(), v_z.end(), random_vel);
	std::generate(mass.begin(), mass.end(), rdis6);
	std::cout << "Finished generate" << std::endl;
}

void saveParticleData(int timestep)
{
	// TODO
	/*
	std::ofstream datafile;
	datafile.open (std::string("data.csv.")+std::to_string(timestep));
	datafile << " x coord,y coord,z coord,scalar\n";
	for (int i = 0; i < N; ++i) {
		datafile << x[i] << ", " << y[i] << ", " << z[i] << ", " << i << "\n";
	}
	datafile.close(); */
	std::ofstream datafile;
	datafile.open ("data.xyz", std::ofstream::out | std::ofstream::app);
	datafile << N << std::endl;
	datafile << "#Comment" << std::endl;
	for (int i = 0; i < N; ++i) {
		datafile << "H " << x[i] << " " << y[i] << " " << z[i] << std::endl;
	}
	datafile.close();
}

} // namespace BH



int main(int argc, char *argv[])
{
	std::cout << "CPU BarnesHut v0.01" << std::endl; //TODO use global version definition

	/*  __gnu_parallel::_Settings s;
	  s.algorithm_strategy = __gnu_parallel::force_parallel;
	  __gnu_parallel::_Settings::set(s); */

#ifdef USE_PARALLEL
	std::cout << "Using parallel mode" << std::endl;
#else
	std::cout << "Using sequential mode" << std::endl;
#endif

	if (true/*argc != 3*/) { // TODO change
		std::cerr << "arguments: number_of_bodies number_of_timesteps" << std::endl;
		//exit(-1);
	}

	po::options_description general_op{"General options"};
	general_op.add_options()
    	    																										("version,v", "print version string")
    	    																										("help,h", "produce help message") // TODO -t flag not working!
    	    																										("timer,t", po::value<bool>(&BH::use_timer)->default_value(false), "measure execution time")
    	    																										; // TODO add verbosity level

	po::options_description physics_op{"Physics options"};
	physics_op.add_options()
			("N", po::value<int>(&BH::N)->default_value(700), "number of stars")
			("timesteps", po::value<int>(&BH::timesteps)->default_value(700), "number of timesteps to simulate")
			// TODO dt
			("min_x", po::value<double>()->default_value(-50000), "minimum x position of created particles")
			("max_x", po::value<double>()->default_value(50000), "maximum x position of created particles")
			("min_y", po::value<double>()->default_value(-50000), "minimum y position of created particles")
			("max_y", po::value<double>()->default_value(50000), "maximum y position of created particles")
			("min_z", po::value<double>()->default_value(-50000), "minimum z position of created particles")
			("max_z", po::value<double>()->default_value(50000), "maximum z position of created particles")
			("min_v_x", po::value<double>()->default_value(-1), "minimum x-velocity of created particles")
			("max_v_x", po::value<double>()->default_value(1), "maximum x-velocity of created particles")
			("min_v_y", po::value<double>()->default_value(-1), "minimum y-velocity of created particles")
			("max_v_y", po::value<double>()->default_value(1), "maximum y-velocity of created particles")
			("min_v_z", po::value<double>()->default_value(-1), "minimum z-velocity of created particles")
			("max_v_z", po::value<double>()->default_value(1), "maximum z-velocity of created particles")
			("min_mass", po::value<double>()->default_value(5), "minimum mass of created particles")
			("max_mass", po::value<double>()->default_value(500000), "maximum mass of created particles")
			;

	po::options_description backend_op{"Backend options"};
	// TODO add timestopping functionality

	po::options_description visible_op{"Allowed options"};
	visible_op.add(general_op).add(physics_op);

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, general_op), vm);
	// TODO make following lines working again
	//std::ifstream ifs{"config.ini"};
	//po::store(po::parse_config_file<char>(ifs, general_op), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << visible_op;
		return 1;
	}

	if (vm.count("version")) {// TODO version in global def header
		std::cout << "0.01" << std::endl;
		return 1;
	}

	// TODO print config data
	for (const auto& it : vm) {
	  std::cout << it.first.c_str() << " ";
	  auto& value = it.second.value();
	  if (auto v = boost::any_cast<uint32_t>(&value))
	    std::cout << *v;
	  else if (auto v = boost::any_cast<std::string>(&value))
	    std::cout << *v;
	  else
	    std::cout << "error";
	  std::cout << std::endl;
	}



	std::cout << "Generate random entrys" << std::endl;
	BH::generateRandomEntrys();
	//	BH::printParticleData();

	// remove particle data // TODO only when xyz is used
	std::system("rm data.xyz");
	std::cout << "Removed existing data.xyz" << std::endl;

	std::chrono::duration<double> treetime(0), massestime(0), acctime(0), updatetime(0), savetime(0);
	std::chrono::high_resolution_clock::time_point start, stop, start_tmp, stop_tmp;
	if(BH::use_timer)
		start = std::chrono::high_resolution_clock::now();
	for(int step = 0; step < BH::timesteps; ++step) {
		std::cout << "Step " << step << std::endl;
#ifdef INFO_MODE
		std::cout << "Build tree" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp = std::chrono::high_resolution_clock::now();
		BH::buildTree(BH::x,BH::y,BH::z,BH::N);
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			treetime += stop_tmp - start_tmp ;
		}
		//printTrees();
		//calcMass of branches
#ifdef INFO_MODE
		std::cout << "Calc node masses" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::calcNodeMasses2();
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			massestime += stop_tmp - start_tmp ;
		}
#ifdef INFO_MODE
		std::cout << "Calc Acc" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::calcAcc(); // TODO make param with lambda expression
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			acctime += stop_tmp - start_tmp ;
		}
#ifdef INFO_MODE
		std::cout << "Update data" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::updateData(); // TODO nur speichern, wenn ein 250 MB groß, o.ä.
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			updatetime += stop_tmp - start_tmp ;
		}


		//printParticleData();
#ifdef INFO_MODE
		std::cout << "Save data" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::saveParticleData(step);
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			savetime += stop_tmp - start_tmp ;
		}
	}
	if(BH::use_timer) {
		auto stop = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> duration =  stop - start;

		std::cout << "Finished in " << duration.count() << " s" << std::endl;
		std::cout << "Tree: " << treetime.count() << " Masses: " << massestime.count() << " Acc: " << acctime.count()
																															<< " Update: " << updatetime.count() << " Save: " << savetime.count() << std::endl;
	}

	return 0;
}
